# admin_page.rb
#
# Note: in the book this is called landing_page

require_relative 'side_menu_panel'

class AdminPage
  include PageObject
  include SideMenuPanel
  
  #page_url('http://puppies.herokuapp.com/admin')
  page_url('http://localhost:3000/admin')
end