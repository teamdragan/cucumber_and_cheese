Given(/^I am on the puppy admin site$/) do
  visit(AdminPage)
  on(LoginPage).login_to_system
end

Given(/^I have a pending adoption for "([^"]*)"$/) do |name|
  on(AdminPage).adopt_puppy
  navigate_to(CheckoutPage).checkout('name' => name)
end

When(/^I process that adoption$/) do 
  on(AdminPage).adoptions 
  on(ProcessPuppyPage).process_first_puppy
end
