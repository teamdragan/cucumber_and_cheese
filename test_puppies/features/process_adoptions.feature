Feature: Processing puppise adoptions

  As a puppy lover
  I want to process puppy adoptions
  So so they can start chewing my furniture asap
  
Background:
  Given I am on the puppy admin site
  
Scenario: Verify message when an adoption is processed
  Given I have a pending adoption for "Tom Jones"
  When I process that adoption
  Then I should see "Please thank Tom Jones for the order!"
