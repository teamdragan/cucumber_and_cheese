module AdoptionHelper

def goto_puppies_site
	@browser = Watir::Browser.new :firefox
	@browser.goto 'http://puppies.herokuapp.com'
end

def adopt_puppy_number(num)
	@browser.button(:value => 'View Details', :index => num-1).click
	@browser.button(:value => 'Adopt Me!').click
end

def continue_adopting_puppies
	@browser.button(:value => 'Adopt Another Puppy').click
end

def complete_adoption(order_name, order_address, order_email, order_pay_type)
	@browser.button(:value => 'Complete the Adoption').click
	@browser.text_field(:id => 'order_name').set(order_name)
	@browser.text_field(:id => 'order_address').set(order_address)
	@browser.text_field(:id => 'order_email').set(order_email)
	@browser.select_list(:id => 'order_pay_type').select(order_pay_type)
	@browser.button(:value => 'Place Order').click
end

def verify_adoption
	fail unless @browser.text.include? 'Thank you for adopting a puppy!'
end

def close_browser
	@browser.close
end


end