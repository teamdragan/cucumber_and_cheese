require 'rubygems'
require 'watir-webdriver'
require_relative 'adoption_helper'

include AdoptionHelper

goto_puppies_site
adopt_puppy_number 1
continue_adopting_puppies
adopt_puppy_number 2
complete_adoption('Cheesy', '123 Main St.', 'cheesy@foo.com', 'Check')
verify_adoption
close_browser
