unless defined?(MAX_LOCATE_ATTEMPTS)
  MAX_LOCATE_ATTEMPTS = 3
  LOCATE_WAIT_TIME_SHORT = 2
  LOCATE_WAIT_TIME_REGULAR = 20
  LOCATE_WAIT_TIME_LONG = 35
end

class HomePage
  include PageObject

  page_url "https://test-backoffice.gamecredits2.org/backoffice/"

  #HomePage elements

  h2(:home_page_header, text: 'Home')
  link(:users_link, text: 'Users')
  link(:games_link, text: 'Games')
  link(:transactions_link, text: 'Transactions')
  link(:audit_logs_link, text: 'Audit Logs')
  link(:bank_info_link, text: 'Bank info')
  div(:users_description_paragraph, class: ['alert', 'alert-info'], index: 0)
  div(:games_description_paragraph, class: ['alert', 'alert-info'], index: 1)
  div(:transactions_description_paragraph, class: ['alert', 'alert-info'], index: 2)
  div(:audit_logs_description_paragraph, class: ['alert', 'alert-info'], index: 3)
  div(:bank_info_description_paragraph, class: ['alert', 'alert-info'], index: 4)
  link(:miner_link, text: 'Miner')
  link(:pool_link, href: '/backoffice/pool/info', index: 1)
  div(:home_page_footer, class: 'container')

  #Search Users elements
  text_field(:search_users_by_account_mail, id: 'searchPhrase')
  button(:search_users_button, class: ['btn', 'btn-default'])

  def provera
    x = self.table_element(class: ['table', 'table-striped'], index: 0)[1][0].text
    y = x.slice!(0..-8)
    w = self.text_field_element(id: 'searchPhrase').value
    if y != w
      return "No Users found for the typed criteria!"
    end
  end

end
