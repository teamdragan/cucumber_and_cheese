unless defined?(MAX_LOCATE_ATTEMPTS)
  MAX_LOCATE_ATTEMPTS = 3
  LOCATE_WAIT_TIME_SHORT = 2
  LOCATE_WAIT_TIME_REGULAR = 20
  LOCATE_WAIT_TIME_LONG = 35
end

class HeaderPage
  include PageObject

  page_url "https://test-backoffice.gamecredits2.org/backoffice/"

  link(:backoffice_sign_out_button, href: '/backoffice/logout')

  def click_on_elements_from_header
    i = 0
    x = self.unordered_list_elements(class: ['nav', 'navbar-nav']).count - 1
    while i < x do
      self.unordered_list_element(class: ['nav', 'navbar-nav'], index: i).click
      i += 1
    end
  end
end
