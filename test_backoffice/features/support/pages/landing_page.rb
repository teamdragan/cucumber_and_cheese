unless defined?(MAX_LOCATE_ATTEMPTS)
  MAX_LOCATE_ATTEMPTS = 3
  LOCATE_WAIT_TIME_SHORT = 2
  LOCATE_WAIT_TIME_REGULAR = 20
  LOCATE_WAIT_TIME_LONG = 35
end

class LandingPage
  include PageObject

  page_url "https://test-backoffice.gamecredits2.org/backoffice/"

  #landing page elements
  h2(:landing_page_header, text: 'Home')
  link(:landing_page_login, text: 'Login')
  link(:landing_page_home, text: 'Home')
  text_field(:user_name, id: 'username')
  text_field(:password, id: 'password')
  button(:sign_in_button, class: ['btn', 'btn-lg', 'btn-primary', 'btn-block'])
  link(:about_panel_link, text: 'About panel')
  div(:landing_page_footer, class: 'container')

  def landing_page_exists?
    self.button_element(class: ['btn', 'btn-lg', 'btn-primary', 'btn-block']).exists?
  end

  def am_i_logged_in
    result = self.link_element(text: 'Login').exists?
    !result
  end
end
