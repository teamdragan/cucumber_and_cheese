# FILE NAME: miner.feature
# NOTES: Testing the Backoffice Miner Link


Feature: Backoffice Miner
  As a Backoffice user
  I want to be able to see miner rewards
  So I can verify mining functionality works correctly

  Background:
    Given I am successfuly logged in to the Backoffice Home Page

  Scenario: Miner link verification
    When I click on the Miner link on Backoffice Home Page
    And I click on option Rewards on Backoffice Miner Page
    Then I should see panel where I can search rewarded emails
