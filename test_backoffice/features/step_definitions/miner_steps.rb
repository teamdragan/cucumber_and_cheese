Given(/^I navigate to the Backoffice page URL$/) do
  visit LandingPage
  on LandingPage do |lpage|
    if lpage.am_i_logged_in
      on HeaderPage do |hpage|
        hpage.backoffice_sign_out_button
      end
    end
  end
end

Given(/^I am successfuly logged in to the Backoffice Home Page$/) do
  step %(I navigate to the Backoffice page URL)
  on LandingPage do |page|
    if not page.am_i_logged_in
      step %(I click on Login button on Landing Page of Backoffice)
      step %(I enter "master" and "xtlJC81SipuSVPym" and click on the Log in button)
      step %(I should see the Backoffice Home Page content)
    end
  end
end

When("I click on Login button on Landing Page of Backoffice") do
  on LandingPage do |page|
    page.landing_page_login
  end
end

When("I enter {string} and {string} and click on the Log in button") do |email, password|
  on LandingPage do |page|
    page.user_name = email
    page.password = password
    page.sign_in_button
  end
end

Then("I should see the Backoffice Home Page content") do
  on HomePage do |page|
    page.home_page_header?
    page.users_link?
    page.games_link?
    page.transactions_link?
    page.audit_logs_link?
    page.bank_info_link?
    page.users_description_paragraph?
    page.games_description_paragraph?
    page.transactions_description_paragraph?
    page.audit_logs_description_paragraph?
    page.bank_info_description_paragraph?
  end
end

When("I click on the Miner link on Backoffice Home Page") do
  on HomePage do |page|
    wait = Selenium::WebDriver::Wait.new(:timeout => 5)
    wait.until { page.miner_link }
  end
end

When("I click on option Rewards on Backoffice Miner Page") do
  on MinerPage do |page|
    page.reward_link
  end
end

Then("I should see panel where I can search rewarded emails") do
  on RewardsPage do |page|
    page.rewards_page_elements
  end
end

