Progress: check git log

Instaling Puppies app locally
=============================
- get latest from github https://github.com/cheezy/puppies
- when you cd to puppies dir you get notice:
  'You are using '.rvmrc', it requires trusting, it is slower ...'
  => You are asked to press Yes, No or Cancel
- Press 'c' to cancel
- Delete .rvmc file
- Make sure you use Ruby 1.9.3 (if needed run 'rvm use 1.9.3)
- Run 'bundle install'
  - if you are unable to install gem 'pq', install postgressql
    using 'sudo apt-get install libpq-dev'
- Run 'bundle exec rake db:migrate'
- Run 'bundle exec rake db:seed'
- Run 'rails s'
- IMPORTANT: DO NOT RUN 'bundle update'
  => if you do you get error:
  'ArgumentError (unsupported parameters: :order):
    app/controllers/agency_controller.rb:6:in `index'

CRAFT
=====

TIP: Part of the lesson here is that it is a very poor web application development practice to create
html elements that do not have an id or name attribute. This is what makes the tests brittle -
unnecessarily brittle. This is when a conversation between a tester and programmer is valuable
- even pair programming between them. It is frequently a trivial amount of work to add id’s and
name’s to elements without them. Much less work, in fact, than trying to maintain test code that
compensates for lack of them!