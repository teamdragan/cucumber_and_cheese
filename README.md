### What is this repository for? ###

* Examples from the book Cucumber and Cheese - A Testers Workshop by Jeff Morgan

### How do I get set up? ###

Install Cucumber depenedencies by runnimg 'bundle install' from the test_pupies
folder.

To execute tests:
  $ cd path/to/test_pupies
  $ cucumber features/<name>.feature
